import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainTypeComponent } from './main-type.component';

describe('MainTypeComponent', () => {
  let component: MainTypeComponent;
  let fixture: ComponentFixture<MainTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
