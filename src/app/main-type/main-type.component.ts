import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-main-type',
  templateUrl: './main-type.component.html',
  styleUrls: ['./main-type.component.css']
})
export class MainTypeComponent implements OnInit {
  public name;
  constructor( private route: ActivatedRoute) { }
  ngOnInit() {
    this.getType();
  }

  getType() {
    this.name = this.route.snapshot.paramMap.get('name');
    return name;
  }
}
