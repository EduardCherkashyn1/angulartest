import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Artist, ArtistService} from '../artist.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core'

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {
  public id: number;
  public artist: Artist;
  constructor(private route: ActivatedRoute, private artistService: ArtistService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getId();
    this.artistService.getArtists()
        .subscribe((result: Array<Artist>) => this.artist = result.find(artist => artist.id === this.id));
  }
  getId() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
  }
  sanitize(Url: string) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(Url);
    }
}
