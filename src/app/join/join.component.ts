import { Component } from '@angular/core';
import {Artist} from '../artist.service';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css'],
})
export class JoinComponent {
  fileToUpload: File = null;
  submitted = false;
  types = ['Musicians', 'Dancers',
    'Singers', 'Ballroom'];
  onSubmit() { this.submitted = true; }

  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this); }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }
}
