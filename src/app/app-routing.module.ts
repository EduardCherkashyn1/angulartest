import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
import { AboutComponent } from './about/about.component';
import { JoinComponent } from './join/join.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MainTypeComponent } from './main-type/main-type.component';
import { OneTypeComponent} from './one-type/one-type.component';
import { ArtistComponent } from './artist/artist.component';

const routes: Routes = [
  { path: 'contacts', component: ContactsComponent },
  { path: 'about', component: AboutComponent },
  { path: 'join', component: JoinComponent },
  { path: 'homepage', component: HomepageComponent },
  { path: 'mainType/:name', component: MainTypeComponent },
  { path: 'oneType/:type', component: OneTypeComponent },
  { path: 'artist/:id', component: ArtistComponent } ,
];
@NgModule({
  exports: [ RouterModule ],
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
