import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface Artist {
  id: number;
  type: string;
  email: string;
  videos: Array<Video>;
  pictures: Array<Picture>;
  biography: string;
}

export interface Video {
  link: string;
}
export interface Picture {
  path: string;
}


@Injectable({
  providedIn: 'root'
})
export class ArtistService {
  private Url = 'assets/artists.json';

  constructor(private http: HttpClient) { }
  getArtists() {
    return this.http.get<Array<Artist>>(this.Url, {responseType : 'json'});
  }
}
