import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Artist, ArtistService} from '../artist.service';

@Component({
  selector: 'app-one-type',
  templateUrl: './one-type.component.html',
  styleUrls: ['./one-type.component.css']
})
export class OneTypeComponent implements OnInit {
  public type;
  public artists: Array<Artist>;
  constructor(private route: ActivatedRoute, private artistService: ArtistService) { }
  ngOnInit() {
     this.artistService.getArtists()
         .subscribe((result: Array<Artist>) => this.artists = result.filter(artist => artist.type === this.getType()));
    }

  getType() {
    this.type = this.route.snapshot.paramMap.get('type');
    return this.type;
  }
}
